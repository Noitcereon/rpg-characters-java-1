# RPG Characters Java 1
RPG Characters Java 1 is a console application, in which you can create characters and equipable items.

It is part of an assignment covering Java fundamentals.

## Installation
You need a JDK (Java Development Kit) installed to run this program.

Aside from the JDK no installation should be neccesary, assuming you use an IDE with the same capabilities as **IntelliJ**.

## Usage
You need **IntelliJ** or a similar IDE to run the code. Alternatively use the command line to compile each java file and run the code, but it is not recommended.

**With the IDE**, run Main in the src folder.

The project also have tests you can run in the tests folder.

Example code:

```
    IMage character = CharacterFactory.CreateMage("Torval");

    character.equipItem(ItemFactory.CreateWand("Wand of Fire", 12, 12, 1));
    character.equipItem(ItemFactory.CreateHeadArmor(ArmorType.CLOTH, "Helmet of Domination Replica", new GameAttributes(100, -2, 100)));

```


## Maintainers
Thomas Andersen (@Noitcereon)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
