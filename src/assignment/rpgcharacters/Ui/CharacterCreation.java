package assignment.rpgcharacters.Ui;

import assignment.rpgcharacters.Factories.CharacterFactory;
import assignment.rpgcharacters.Factories.ItemFactory;
import assignment.rpgcharacters.Models.Enums.ArmorType;
import assignment.rpgcharacters.Models.GameAttributes;
import assignment.rpgcharacters.Models.Interfaces.ICharacter;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Scanner;

public class CharacterCreation {

    public static ICharacter startCharacterCreation() {
        Scanner scanner = new Scanner(System.in);

        // Ask for class
        String selectedClass = classSelection(scanner);
        // Ask for name
        String selectedName = nameSelection(scanner);
        // Create character of selectedClass & name.
        // Equip starter items.
        ICharacter character = null;
        switch (Objects.requireNonNull(selectedClass)) {
            case "warrior" -> character = CharacterFactory.CreateWarrior(selectedName);
            case "mage" -> character = CharacterFactory.CreateMage(selectedName);
            case "rogue" -> character = CharacterFactory.CreateRogue(selectedName);
            case "ranger" -> {
                character = CharacterFactory.CreateRanger(selectedName);
                equipStarterGear(character);
            }
            default -> {
                System.out.println("Invalid input, restarting character creation process.");
                startCharacterCreation();
            }
        }

        return character;
    }

    private static void equipStarterGear(ICharacter character) {
        switch (character.getCharacterClass()) {
            case "warrior" -> {
                character.equipItem(ItemFactory.CreateDagger("Plain Axe", 2, 4, 0.8));
                character.equipItem(ItemFactory.CreateBodyArmor(ArmorType.MAIL, "Worn Chain Mail", new GameAttributes(1, 0, 0)));
                character.equipItem(ItemFactory.CreateLegArmor(ArmorType.LEATHER, "Comfy Leather Pants", new GameAttributes(0, 1, 0)));
            }
            case "mage" -> {
                character.equipItem(ItemFactory.CreateDagger("Staff", 3, 5, 2));
                character.equipItem(ItemFactory.CreateBodyArmor(ArmorType.CLOTH, "Comfy Chest Piece", new GameAttributes(0, 0, 2)));
                character.equipItem(ItemFactory.CreateLegArmor(ArmorType.CLOTH, "Snug Pants", new GameAttributes(0, 1, 2)));
            }
            case "rogue" -> {
                character.equipItem(ItemFactory.CreateDagger("Thief's Dagger", 1, 3, 1));
                character.equipItem(ItemFactory.CreateBodyArmor(ArmorType.LEATHER, "Thief's Leather Armor", new GameAttributes(0, 2, 0)));
                character.equipItem(ItemFactory.CreateLegArmor(ArmorType.LEATHER, "Thief's Leather Pants", new GameAttributes(0, 1, 0)));
            }
            case "ranger" -> {
                character.equipItem(ItemFactory.CreateBow("Apprentice's Bow", 1, 3, 0.7));
                character.equipItem(ItemFactory.CreateDagger("Apprentice's Dagger", 1, 2, 0.9));
                character.equipItem(ItemFactory.CreateBodyArmor(ArmorType.LEATHER, "Apprentice's Leather Armor", new GameAttributes(0, 1, 0)));
                character.equipItem(ItemFactory.CreateLegArmor(ArmorType.LEATHER, "Apprentice's Leather Pants", new GameAttributes(0, 1, 0)));
            }
        }
    }

    private static String nameSelection(Scanner scanner) {
        System.out.println("Enter name: ");
        String name = scanner.nextLine();
        if (name.isBlank()) {
            System.out.println("Invalid name, try again.");
            nameSelection(scanner);
        }
        System.out.println(name + " was selected");
        return name;
    }

    private static String classSelection(Scanner scanner) {
        String userClassSelection;
        List<String> availableClasses = CharacterFactory.GetAvailableClasses();
        System.out.println("Enter the name of a class listed below: ");
        availableClasses.forEach(c -> System.out.println("- " + c));
        userClassSelection = scanner.nextLine().toLowerCase(Locale.ROOT);
        switch (userClassSelection) {
            case "mage", "warrior", "rogue", "ranger":
                System.out.println(userClassSelection + " was selected.");
                return userClassSelection;
            case "c":
                System.out.println("Cancelling character creation");
            default:
                System.out.println("Invalid class. Enter 'c' if you to cancel character creation.");
                classSelection(scanner);
        }
        // This shouldn't be reached
        return null;
    }
}
