package assignment.rpgcharacters.Exceptions;

public class InvalidArmorException extends IllegalArgumentException {
    public InvalidArmorException(){
        super();
    }
    public InvalidArmorException(String message){
        super(message);
    }
}
