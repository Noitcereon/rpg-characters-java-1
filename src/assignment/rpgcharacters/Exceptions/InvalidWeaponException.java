package assignment.rpgcharacters.Exceptions;

import java.security.InvalidParameterException;

public class InvalidWeaponException extends IllegalArgumentException{
   public InvalidWeaponException(){
       super();
   }
   public InvalidWeaponException(String message){
       super(message);
   }
}
