package assignment.rpgcharacters.Models.BaseClasses;

import assignment.rpgcharacters.Models.Enums.ItemSlot;
import assignment.rpgcharacters.Models.Interfaces.IItem;

import java.util.UUID;

public class BaseItem implements IItem {
    private ItemSlot itemSlot;
    private UUID id;
    private String name;
    private final int itemLevel;

    protected BaseItem(ItemSlot slot, String name, int itemLevel) {
        setId();
        setItemSlot(slot);
        setName(name);
        this.itemLevel = itemLevel;
    }

    public UUID getId() {
        return id;
    }

    private void setId() {
        this.id = UUID.randomUUID();
    }

    public ItemSlot getItemSlot() {
        return itemSlot;
    }

    protected void setItemSlot(ItemSlot itemSlot) {
        this.itemSlot = itemSlot;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getItemLevel() {
        return this.itemLevel;
    }


}
