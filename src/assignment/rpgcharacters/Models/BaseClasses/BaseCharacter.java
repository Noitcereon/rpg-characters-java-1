package assignment.rpgcharacters.Models.BaseClasses;

import assignment.rpgcharacters.Exceptions.InvalidArmorException;
import assignment.rpgcharacters.Exceptions.InvalidWeaponException;
import assignment.rpgcharacters.Models.Armor;
import assignment.rpgcharacters.Models.Enums.ArmorType;
import assignment.rpgcharacters.Models.Enums.ItemSlot;
import assignment.rpgcharacters.Models.Enums.WeaponType;
import assignment.rpgcharacters.Models.GameAttributes;
import assignment.rpgcharacters.Models.Interfaces.ICharacter;
import assignment.rpgcharacters.Models.Interfaces.IItem;
import assignment.rpgcharacters.Models.Weapon;

import java.util.Arrays;
import java.util.HashMap;

public abstract class BaseCharacter implements ICharacter {

    // region Private Fields
    private String name;
    private String characterClass;
    private int level;
    private GameAttributes baseAttributes;
    private GameAttributes totalAttributes; // calculated with equipment
    private final GameAttributes levelUpStatIncrement;
    private final WeaponType[] validWeapons;
    private final ArmorType[] validArmor;
    private final HashMap<ItemSlot, IItem> equipment;
    // endregion Private Fields

    protected BaseCharacter(String name, String characterClass, GameAttributes baseAttributes, GameAttributes levelUpStatIncrement, ArmorType[] validArmor, WeaponType[] validWeapons) {
        setCharacterClass(characterClass);
        setLevel(1);
        setName(name);
        this.levelUpStatIncrement = levelUpStatIncrement;
        setBaseAttributes(baseAttributes);
        this.validWeapons = validWeapons;
        this.validArmor = validArmor;
        this.equipment = new HashMap<ItemSlot, IItem>();
        setTotalAttributes(new GameAttributes(baseAttributes));
    }

    // region Properties

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharacterClass() {
        return characterClass;
    }

    protected void setCharacterClass(String characterClass) {
        this.characterClass = characterClass;
    }

    public int getLevel() {
        return level;
    }

    protected void setLevel(int level) {
        this.level = level;
    }

    public GameAttributes getBaseAttributes() {
        return baseAttributes;
    }

    protected void setBaseAttributes(GameAttributes baseStats) {
        this.baseAttributes = baseStats;
    }

    public GameAttributes getTotalAttributes() {
        return totalAttributes;
    }

    protected void setTotalAttributes(GameAttributes totalAttributes) {
        this.totalAttributes = totalAttributes;
    }

    protected void updateTotalAttributes(GameAttributes modificationToAttributes) {
        this.totalAttributes.updateAllStats(modificationToAttributes);
    }

    // endregion Properties

    // region Methods

    /**
     * Should increase base- and total- attributes with a certain amount based on character class.
     */
    public void levelUp() {
        int level = getLevel();
        this.baseAttributes.updateAllStats(this.levelUpStatIncrement);
        this.totalAttributes.updateAllStats(this.levelUpStatIncrement);
        setLevel(++level);
    }

    /**
     * @param item The item to equip
     * @return true if the item was equipped. Otherwise, false.
     */
    public boolean equipItem(IItem item) {
        if (item.getItemLevel() > getLevel()) {
            if (item.getItemSlot() == ItemSlot.WEAPON) {
                throw new InvalidWeaponException("Weapon too high level. Cannot equip.");
            }
            throw new InvalidArmorException("Armor too high level. Cannot equip.");
        }
        // any other item (armor)
        if (item.getItemSlot() == ItemSlot.WEAPON) {
            Weapon weapon = (Weapon) item;
            WeaponType weaponType = weapon.getWeaponType();
            if (Arrays.stream(validWeapons).anyMatch((wpType -> wpType == weaponType))) {
                equipment.put(ItemSlot.WEAPON, weapon);
                return true;
            }
            throw new InvalidWeaponException("Cannot equip this weapon type");
        } else {
            Armor armor = (Armor) item;
            ArmorType armorType = armor.getArmorType();
            if (Arrays.stream(validArmor).anyMatch((amrType -> amrType == armorType))) {
                equipment.put(armor.getItemSlot(), armor);
                updateTotalAttributes(armor.getArmorStats());
                return true;
            }
            throw new InvalidArmorException("Cannot equip this armor type");
        }
    }

    public int calculateDps() {
        // Weapon DPS * (1 + TotalMainPrimaryAttribute/100)
        double percentDmgIncrease = (1 + (getTotalAttributes().getPrimaryAttributeValue() / 100f));
        if (equipment.containsKey(ItemSlot.WEAPON)) {
            Weapon weapon = (Weapon) equipment.get(ItemSlot.WEAPON);
            return (int) (weapon.getWeaponDps() * percentDmgIncrease);
        } else {
            // if no weapon is present Weapon DPS is '1'
            return (1 + totalAttributes.getPrimaryAttributeValue() / 100);
        }
    }

    // endregion Methods


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        GameAttributes totalStats = getTotalAttributes();

        sb.append("Name: ").append(this.name);
        sb.append('\n');
        sb.append("Class: ").append(this.characterClass);
        sb.append('\n');
        sb.append("Level: ").append(this.level);
        sb.append('\n');
        sb.append("Strength: ").append(totalStats.getStrength());
        sb.append('\n');
        sb.append("Dexterity: ").append(totalStats.getDexterity());
        sb.append('\n');
        sb.append("Intelligence: ").append(totalStats.getIntelligence());
        sb.append('\n');

        return sb.toString();
    }
}
