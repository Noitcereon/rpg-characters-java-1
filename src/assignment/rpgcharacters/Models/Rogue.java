package assignment.rpgcharacters.Models;

import assignment.rpgcharacters.Models.BaseClasses.BaseCharacter;
import assignment.rpgcharacters.Models.Enums.ArmorType;
import assignment.rpgcharacters.Models.Enums.WeaponType;
import assignment.rpgcharacters.Models.Interfaces.IItem;
import assignment.rpgcharacters.Models.Interfaces.IRogue;

public class Rogue extends BaseCharacter implements IRogue {
    public Rogue(String name, GameAttributes baseAttributes, GameAttributes levelUpStatIncrement,
                 ArmorType[] validArmor, WeaponType[] validWeapons) {
        super(name, "Rogue", baseAttributes, levelUpStatIncrement, validArmor, validWeapons);
    }
}
