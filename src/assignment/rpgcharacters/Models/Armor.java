package assignment.rpgcharacters.Models;


import assignment.rpgcharacters.Models.BaseClasses.BaseItem;
import assignment.rpgcharacters.Models.Enums.ArmorType;
import assignment.rpgcharacters.Models.Enums.ItemSlot;

public class Armor extends BaseItem {


    private final ArmorType armorType;
    private final GameAttributes armorStats;

    /**
     * Armor constructor with default item level = 1.
     *
     * @param slot The item slot it fits in. HEAD, LEGS or other
     * @param name Name of the item.
     */
    public Armor(ItemSlot slot, ArmorType armorType, String name, GameAttributes armorStats) {
        super(slot, name, 1);
        this.armorStats = armorStats;
        this.armorType = armorType;
    }

    public Armor(ItemSlot slot, ArmorType armorType, String name, int itemLevel, GameAttributes armorStats) {
        super(slot, name, itemLevel);
        this.armorStats = armorStats;
        this.armorType = armorType;
    }

    // Getters & Setters

    public ArmorType getArmorType() {
        return armorType;
    }

    public GameAttributes getArmorStats() {
        return armorStats;
    }

    // endregion Getters and Setters
}
