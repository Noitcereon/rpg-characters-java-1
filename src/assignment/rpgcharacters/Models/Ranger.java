package assignment.rpgcharacters.Models;

import assignment.rpgcharacters.Models.BaseClasses.BaseCharacter;
import assignment.rpgcharacters.Models.Enums.ArmorType;
import assignment.rpgcharacters.Models.Enums.WeaponType;
import assignment.rpgcharacters.Models.Interfaces.IItem;
import assignment.rpgcharacters.Models.Interfaces.IRanger;

public class Ranger extends BaseCharacter implements IRanger {
    public Ranger(String name, GameAttributes baseAttributes, GameAttributes levelUpStatIncrement,
                  ArmorType[] validArmor, WeaponType[] validWeapons) {
        super(name, "Ranger", baseAttributes, levelUpStatIncrement, validArmor, validWeapons);
    }

}
