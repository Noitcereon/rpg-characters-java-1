package assignment.rpgcharacters.Models;

import assignment.rpgcharacters.Models.BaseClasses.BaseItem;
import assignment.rpgcharacters.Models.Enums.ItemSlot;
import assignment.rpgcharacters.Models.Enums.WeaponType;

import java.security.InvalidParameterException;

public class Weapon extends BaseItem {
    private int minDmg;
    private int maxDmg;
    private double attackSpeed;
    private WeaponType weaponType;

    public Weapon(WeaponType weaponType, String name, int itemLevel, int minDmg,
                  int maxDmg, double attackSpeed) {
        super(ItemSlot.WEAPON, name, itemLevel);
        setDamage(minDmg, maxDmg);
        setAttackSpeed(attackSpeed);
        setWeaponType(weaponType);
    }

    /**
     * Weapon constructor with a default item level = 1.
     */
    public Weapon(WeaponType weaponType, String name, int minDmg, int maxDmg, double attackSpeed) {
        super(ItemSlot.WEAPON, name, 1);
        setDamage(minDmg, maxDmg);
        setAttackSpeed(attackSpeed);
        setWeaponType(weaponType);
    }

    /**
     * @return A number between minDmg and maxDmg (both inclusive).
     */
    public int getWeaponDamage() {
        // Generate a number between min and max dmg. +1 to make maxDmg inclusive.
        return (int) (Math.random() * (maxDmg + 1 - minDmg) + minDmg);
    }

    /**
     * @return WeaponDamage * attack speed.
     */
    public int getWeaponDps() {
        return (int) (getWeaponDamage() * this.attackSpeed);
    }

    public int getMinDmg() {
        return this.minDmg;
    }

    public int getMaxDmg() {
        return this.maxDmg;
    }

    public void setDamage(int minDmg, int maxDmg) {
        if ((minDmg > maxDmg) || minDmg < 0) {
            throw new InvalidParameterException("minDmg is less than 0 or maxDmg is less than minDmg");
        }
        this.minDmg = minDmg;
        this.maxDmg = maxDmg;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    /**
     * Sets the weapons attack speed. Max is 10 attacks per second.
     *
     * @param attackSpeed Attacks per second
     */
    public void setAttackSpeed(double attackSpeed) {
        // Max 10 attacks per second
        if (attackSpeed <= 10 && attackSpeed > 0) {
            this.attackSpeed = attackSpeed;
            return;
        }
        throw new InvalidParameterException("Attack speed should be between 0 and 10");
    }

    /**
     * WeaponType, e.g. Sword, Axe or Bow.
     *
     * @return The type of the weapon.
     */
    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }
}
