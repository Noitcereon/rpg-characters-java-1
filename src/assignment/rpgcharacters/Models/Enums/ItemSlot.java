package assignment.rpgcharacters.Models.Enums;

public enum ItemSlot {
    WEAPON,
    HEAD,
    BODY,
    LEGS
}
