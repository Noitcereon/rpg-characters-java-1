package assignment.rpgcharacters.Models.Enums;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
