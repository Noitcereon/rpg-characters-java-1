package assignment.rpgcharacters.Models.Enums;

/**
 * Holds the Weapon Types in the game, such as AXE, BOW, WAND or SWORD.
 */
public enum WeaponType {
    AXE,
    BOW,
    DAGGER,
    HAMMER,
    STAFF,
    SWORD,
    WAND
}
