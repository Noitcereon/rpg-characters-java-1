package assignment.rpgcharacters.Models.Enums;

public enum PrimaryAttribute {
    STRENGTH,
    DEXTERITY,
    INTELLIGENCE
}
