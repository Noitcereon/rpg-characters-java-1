package assignment.rpgcharacters.Models.Interfaces;

import assignment.rpgcharacters.Models.GameAttributes;

/**
 * Represents the basic implementation of all Characters.
 */
public interface ICharacter {
    /**
     *
     * @return Name of the character.
     */
    String getName();

    /**
     * Updates the Character's name with the name parameter.
     * @param name The new name
     */
    void setName(String name);

    /**
     *
     * @return Name of the character's class.
     */
    String getCharacterClass();

    /**
     *
     * @return Character's current level.
     */
    int getLevel();

    /**
     *
     * @return Character's current base attributes
     */
    GameAttributes getBaseAttributes();

    /**
     * Adds base attributes + bonuses added from armor into one CharacterStats object.
     * @return CharacterStats object with calculated total attributes.
     */
    GameAttributes getTotalAttributes();

    /**
     * Increases character level by 1 and increases base attributes
     */
    void levelUp();


    /**
     * Equips an item if the character fulfills requirements and updates their totalStats.
     */
    boolean equipItem(IItem item);

    /**
     * Returns the damage dealt as an integer.
     */
    int calculateDps();

}
