package assignment.rpgcharacters.Models.Interfaces;

import assignment.rpgcharacters.Models.Enums.ItemSlot;

import java.util.UUID;

public interface IItem {
    UUID getId();
    ItemSlot getItemSlot();
    String getName();
    int getItemLevel();
}
