package assignment.rpgcharacters.Models;

import assignment.rpgcharacters.Models.BaseClasses.BaseCharacter;
import assignment.rpgcharacters.Models.Enums.ArmorType;
import assignment.rpgcharacters.Models.Enums.WeaponType;
import assignment.rpgcharacters.Models.Interfaces.IItem;
import assignment.rpgcharacters.Models.Interfaces.IWarrior;

public class Warrior extends BaseCharacter implements IWarrior {
    public Warrior(String name, GameAttributes baseAttributes, GameAttributes levelUpStatIncrement,
                   ArmorType[] validArmor, WeaponType[] validWeapons) {
        super(name, "Warrior", baseAttributes, levelUpStatIncrement, validArmor, validWeapons);
    }
}
