package assignment.rpgcharacters.Models;

import assignment.rpgcharacters.Models.Enums.PrimaryAttribute;

public class GameAttributes {

    private int strength;
    private int dexterity;
    private int intelligence;
    private PrimaryAttribute primaryAttribute;

    public GameAttributes(GameAttributes existingStats) {
        setStrength(existingStats.getStrength());
        setDexterity(existingStats.getDexterity());
        setIntelligence(existingStats.getIntelligence());
        setPrimaryAttribute(existingStats.getPrimaryAttribute());
    }

    /**
     * Sets the 3 attributes without a primary attribute.
     *
     * @param strength     str
     * @param dexterity    dex
     * @param intelligence int
     */
    public GameAttributes(int strength, int dexterity, int intelligence) {
        setStrength(strength);
        setDexterity(dexterity);
        setIntelligence(intelligence);
    }

    public GameAttributes(PrimaryAttribute primaryAttribute, int strength, int dexterity, int intelligence) {
        setPrimaryAttribute(primaryAttribute);
        setStrength(strength);
        setDexterity(dexterity);
        setIntelligence(intelligence);
    }


    public PrimaryAttribute getPrimaryAttribute() {
        return primaryAttribute;
    }

    /**
     * @return The integer value associated with the primary attribute.
     */
    public int getPrimaryAttributeValue() {
        switch (this.primaryAttribute) {
            case STRENGTH -> {
                return this.strength;
            }
            case DEXTERITY -> {
                return this.dexterity;
            }
            case INTELLIGENCE -> {
                return this.intelligence;
            }
            default -> {
                System.out.println("PrimaryAttribute not found");
                return -1;
            }
        }
    }

    public void setPrimaryAttribute(PrimaryAttribute primaryAttribute) {
        this.primaryAttribute = primaryAttribute;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {

        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public void updateAllStats(GameAttributes attributeModification) {
        // strength
        if (attributeModification.getStrength() + this.strength < 0) {
            this.strength = 0;
        } else {
            setStrength(this.strength + attributeModification.getStrength());
        }
        // dexterity
        if (attributeModification.getDexterity() + this.dexterity < 0) {
            this.dexterity = 0;
        } else {
            setDexterity(this.dexterity + attributeModification.getDexterity());
        }
        // intelligence
        if (attributeModification.getIntelligence() + this.intelligence < 0) {
            this.intelligence = 0;
        } else {
            setIntelligence(this.intelligence + attributeModification.getIntelligence());
        }
    }


    // region equals

    /**
     * Compares the values of two CharacterStats objects (minus the primary attribute)
     *
     * @param statsObject the CharacterStats object to compare to.
     * @return true, if all stats are the same. Otherwise, false.
     */
    public boolean equals(GameAttributes statsObject) {
        boolean isStrengthEqual = this.strength == statsObject.getStrength();
        boolean isDexterityEqual = this.dexterity == statsObject.getDexterity();
        boolean isIntelligenceEqual = this.intelligence == statsObject.getIntelligence();

        // create array with each check to loop through and check if false.
        boolean[] equalityChecks = {isStrengthEqual, isDexterityEqual, isIntelligenceEqual};
        for (boolean equalityCheck : equalityChecks) {
            if (equalityCheck == false) {
                // return early if false detected
                return false;
            }
        }
        return true;
    }

    /**
     * Compares the values of two CharacterStats objects.
     *
     * @param statsObject           the CharacterStats object to compare to.
     * @param includingPrimaryCheck if true, it will check if the primary attribute is the same.
     * @return true, if all stats are the same. Otherwise, false.
     */
    public boolean equals(GameAttributes statsObject, boolean includingPrimaryCheck) {
        boolean isPrimaryAttributeEqual = true;
        if (includingPrimaryCheck) {
            isPrimaryAttributeEqual = this.primaryAttribute == statsObject.primaryAttribute;
        }
        boolean isStrengthEqual = this.strength == statsObject.getStrength();
        boolean isDexterityEqual = this.dexterity == statsObject.getDexterity();
        boolean isIntelligenceEqual = this.intelligence == statsObject.getIntelligence();

        // create array with each check to loop through and check if false.
        boolean[] equalityChecks = {isPrimaryAttributeEqual, isStrengthEqual, isDexterityEqual, isIntelligenceEqual};
        for (boolean equalityCheck : equalityChecks) {
            if (equalityCheck == false) {
                // return early if false detected
                return false;
            }
        }
        return true;
    }
    // endregion equals
}
