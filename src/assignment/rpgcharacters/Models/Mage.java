package assignment.rpgcharacters.Models;

import assignment.rpgcharacters.Models.BaseClasses.BaseCharacter;
import assignment.rpgcharacters.Models.Enums.ArmorType;
import assignment.rpgcharacters.Models.Enums.ItemSlot;
import assignment.rpgcharacters.Models.Enums.WeaponType;
import assignment.rpgcharacters.Models.Interfaces.IItem;
import assignment.rpgcharacters.Models.Interfaces.IMage;

public class Mage extends BaseCharacter implements IMage {
    public Mage(String name, GameAttributes baseAttributes, GameAttributes levelUpStatIncrement,
                ArmorType[] validArmor, WeaponType[] validWeapons) {
        super(name, "Mage", baseAttributes, levelUpStatIncrement, validArmor, validWeapons);
    }
}
