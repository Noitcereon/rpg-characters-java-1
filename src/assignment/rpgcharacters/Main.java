package assignment.rpgcharacters;

import assignment.rpgcharacters.Factories.CharacterFactory;
import assignment.rpgcharacters.Factories.ItemFactory;
import assignment.rpgcharacters.Models.Enums.ArmorType;
import assignment.rpgcharacters.Models.GameAttributes;
import assignment.rpgcharacters.Models.Interfaces.ICharacter;
import assignment.rpgcharacters.Models.Interfaces.IMage;
import assignment.rpgcharacters.Ui.CharacterCreation;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ICharacter character = CharacterCreation.startCharacterCreation();
        gameLoop(character);
    }

    public static void gameLoop(ICharacter character) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type 'help' to see available commands");
        String userInput;
        do {
            System.out.println("Enter command: ");
            userInput = scanner.nextLine().toLowerCase(Locale.ROOT);
            switch (userInput){
                case "help":
                    System.out.println("Available commands:");
                    availableCommands().forEach(System.out::println);
                    break;
                case "lvl":
                    character.levelUp();
                    System.out.println(character.getName() + " leveled up!");
                    System.out.println(character);
                    break;
                case "inspect":
                    System.out.println(character);
                    break;
                case "new character":
                    character = CharacterCreation.startCharacterCreation();
                    break;
                case "quit", "q", "exit", "stop":
                    break;
                default:
                    System.out.println("Unknown command. Type 'help' to see available commands.");
            }
        } while (isNotQuitCommand(userInput));
        System.out.println("Exiting program...");
    }

    private static boolean isNotQuitCommand(String userInput) {
        return !userInput.equals("quit") && !userInput.equals("q") && !userInput.equals("exit") && !userInput.equals("stop");
    }
    private static List<String> availableCommands(){
        List<String> commands = new ArrayList<>();
        commands.add("help");
        commands.add("lvl");
        commands.add("quit | q | exit | stop");
        commands.add("inspect");
        commands.add("new character");
        return commands;
    }
}
