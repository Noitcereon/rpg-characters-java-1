package assignment.rpgcharacters.Factories;

import assignment.rpgcharacters.Models.*;
import assignment.rpgcharacters.Models.Enums.ArmorType;
import assignment.rpgcharacters.Models.Enums.PrimaryAttribute;
import assignment.rpgcharacters.Models.Enums.WeaponType;
import assignment.rpgcharacters.Models.Interfaces.IMage;
import assignment.rpgcharacters.Models.Interfaces.IRanger;
import assignment.rpgcharacters.Models.Interfaces.IRogue;
import assignment.rpgcharacters.Models.Interfaces.IWarrior;

import java.util.ArrayList;
import java.util.List;

public class CharacterFactory {

    public static IMage CreateMage(String name) {
        GameAttributes baseAttributes = new GameAttributes(PrimaryAttribute.INTELLIGENCE, 1, 1, 8);
        GameAttributes levelUpStatIncrement = new GameAttributes(1, 1, 5);
        ArmorType[] validArmor = {
                ArmorType.CLOTH
        };
        WeaponType[] validWeapons = {WeaponType.WAND, WeaponType.STAFF};

        return new Mage(name, baseAttributes, levelUpStatIncrement, validArmor, validWeapons);
    }

    public static IWarrior CreateWarrior(String name) {
        GameAttributes baseAttributes = new GameAttributes(PrimaryAttribute.STRENGTH, 5, 2, 1);
        GameAttributes levelUpStatIncrement = new GameAttributes(3, 2, 1);
        ArmorType[] validArmor = {
                ArmorType.MAIL,
                ArmorType.PLATE
        };
        WeaponType[] validWeapons = {WeaponType.SWORD, WeaponType.AXE, WeaponType.HAMMER};

        return new Warrior(name, baseAttributes, levelUpStatIncrement, validArmor, validWeapons);
    }

    public static IRogue CreateRogue(String name) {
        GameAttributes baseAttributes = new GameAttributes(PrimaryAttribute.DEXTERITY, 2, 6, 1);
        GameAttributes levelUpStatIncrement = new GameAttributes(1, 4, 1);
        ArmorType[] validArmor = {
                ArmorType.LEATHER,
                ArmorType.MAIL
        };
        WeaponType[] validWeapons = {WeaponType.DAGGER, WeaponType.SWORD};
        return new Rogue(name, baseAttributes, levelUpStatIncrement, validArmor, validWeapons);
    }

    public static IRanger CreateRanger(String name) {
        GameAttributes baseAttributes = new GameAttributes(PrimaryAttribute.DEXTERITY, 1, 7, 1);
        GameAttributes levelUpStatIncrement = new GameAttributes(1, 5, 1);
        ArmorType[] validArmor = {
                ArmorType.LEATHER,
                ArmorType.MAIL
        };
        WeaponType[] validWeapons = {WeaponType.BOW};

        return new Ranger(name, baseAttributes, levelUpStatIncrement, validArmor, validWeapons);
    }
    public static List<String> GetAvailableClasses(){
        // This should correlate to the classes you can make with this factory.
        List<String> classes = new ArrayList<>();
        classes.add("Mage");
        classes.add("Rogue");
        classes.add("Warrior");
        classes.add("Ranger");

        return classes;
    }
}
