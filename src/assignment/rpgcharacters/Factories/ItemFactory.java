package assignment.rpgcharacters.Factories;

import assignment.rpgcharacters.Models.Armor;
import assignment.rpgcharacters.Models.Enums.ArmorType;
import assignment.rpgcharacters.Models.Enums.ItemSlot;
import assignment.rpgcharacters.Models.Enums.WeaponType;
import assignment.rpgcharacters.Models.GameAttributes;
import assignment.rpgcharacters.Models.Weapon;

public class ItemFactory {
    public static Weapon CreateAxe(String name, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.AXE, name, minDmg, maxDmg, atkSpeed);
    }

    public static Weapon CreateAxe(String name, int itemLevel, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.AXE, name, itemLevel, minDmg, maxDmg, atkSpeed);
    }

    public static Weapon CreateBow(String name, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.BOW, name, minDmg, maxDmg, atkSpeed);
    }

    public static Weapon CreateBow(String name, int itemLevel, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.BOW, name, itemLevel, minDmg, maxDmg, atkSpeed);
    }

    public static Weapon CreateDagger(String name, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.DAGGER, name, minDmg, maxDmg, atkSpeed);
    }

    public static Weapon CreateDagger(String name, int itemLevel, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.DAGGER, name, itemLevel, minDmg, maxDmg, atkSpeed);
    }

    public static Weapon CreateHammer(String name, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.HAMMER, name, minDmg, maxDmg, atkSpeed);
    }

    public static Weapon CreateHammer(String name, int itemLevel, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.HAMMER, name, itemLevel, minDmg, maxDmg, atkSpeed);
    }

    public static Weapon CreateStaff(String name, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.STAFF, name, minDmg, maxDmg, atkSpeed);
    }

    public static Weapon CreateStaff(String name, int itemLevel, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.STAFF, name, itemLevel, minDmg, maxDmg, atkSpeed);
    }

    public static Weapon CreateSword(String name, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.SWORD, name, minDmg, maxDmg, atkSpeed);
    }

    public static Weapon CreateSword(String name, int itemLevel, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.SWORD, name, itemLevel, minDmg, maxDmg, atkSpeed);
    }

    public static Weapon CreateWand(String name, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.WAND, name, minDmg, maxDmg, atkSpeed);
    }

    public static Weapon CreateWand(String name, int itemLevel, int minDmg, int maxDmg, double atkSpeed) {
        return new Weapon(WeaponType.WAND, name, itemLevel, minDmg, maxDmg, atkSpeed);
    }
    public static Armor CreateHeadArmor(ArmorType armorType, String name, GameAttributes armorStats){
        return new Armor(ItemSlot.HEAD, armorType, name, armorStats);
    }
    public static Armor CreateHeadArmor(ArmorType armorType, String name, int itemLevel, GameAttributes armorStats){
        return new Armor(ItemSlot.HEAD, armorType, name, itemLevel, armorStats);
    }
    public static Armor CreateBodyArmor(ArmorType armorType, String name, GameAttributes armorStats) {
        return new Armor(ItemSlot.BODY, armorType, name, armorStats);
    }
    public static Armor CreateBodyArmor(ArmorType armorType, String name, int itemLevel, GameAttributes armorStats){
        return new Armor(ItemSlot.BODY, armorType, name, itemLevel, armorStats);
    }
    public static Armor CreateLegArmor(ArmorType armorType, String name, GameAttributes armorStats){
        return new Armor(ItemSlot.LEGS, armorType, name, armorStats);
    }
    public static Armor CreateLegArmor(ArmorType armorType, String name, int itemLevel, GameAttributes armorStats){
        return new Armor(ItemSlot.LEGS, armorType, name, itemLevel, armorStats);
    }


//    public Armor CreateArmor(ItemSlot slot, ) {
//
//    }
}
