package assignment.rpgcharacters.Models;

import assignment.rpgcharacters.Factories.CharacterFactory;
import assignment.rpgcharacters.Factories.ItemFactory;
import assignment.rpgcharacters.Models.Interfaces.IItem;
import assignment.rpgcharacters.Models.Interfaces.IMage;
import assignment.rpgcharacters.Models.Interfaces.IWarrior;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {
    private IWarrior warrior;

    @BeforeEach
    void setUp() {
        warrior = CharacterFactory.CreateWarrior("Nobus");
    }

    @Test
    void equipItem_validInput_returnsTrue() {
        // arrange
        IItem myWeapon = ItemFactory.CreateAxe("Battleaxe", 1,5, 7, 0.5);
        boolean expected = true;
        // act
        boolean actual = warrior.equipItem(myWeapon);

        Assertions.assertEquals(expected, actual);
    }
}