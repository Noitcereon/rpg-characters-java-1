package assignment.rpgcharacters.Models;

import assignment.rpgcharacters.Models.Enums.ArmorType;
import assignment.rpgcharacters.Models.Enums.ItemSlot;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ArmorTest {

    // test constructor
    @Test
    void armorConstructor_validInput_createsArmorWithExpectedValues(){
        ItemSlot expectedItemSlot = ItemSlot.HEAD;
        ArmorType expectedArmorType = ArmorType.LEATHER;
        String expectedItemName = "Leather Helmet";
        GameAttributes expectedArmorStats = new GameAttributes(1, 1, 1);
        int expectedItemLevel = 1;
        Armor armor = new Armor(expectedItemSlot, expectedArmorType, expectedItemName, expectedArmorStats);

        ItemSlot actualItemSlot = armor.getItemSlot();
        ArmorType actualArmorType = armor.getArmorType();
        GameAttributes actualArmorStats = armor.getArmorStats();
        String actualArmorName = armor.getName();
        int actualItemLevel = armor.getItemLevel();

        Assertions.assertTrue(expectedArmorStats.equals(actualArmorStats));
        Assertions.assertEquals(expectedArmorType, actualArmorType);
        Assertions.assertEquals(expectedItemSlot, actualItemSlot);
        Assertions.assertEquals(expectedItemName, actualArmorName);
        Assertions.assertEquals(expectedItemLevel, actualItemLevel);
    }
    @Test
    void armorConstructorWithItemLevel_validInput_createsArmorWithExpectedValues(){
        ItemSlot expectedItemSlot = ItemSlot.HEAD;
        ArmorType expectedArmorType = ArmorType.LEATHER;
        String expectedItemName = "Leather Helmet";
        GameAttributes expectedArmorStats = new GameAttributes(1, 1, 1);
        int expectedItemLevel = 2;
        Armor armor = new Armor(expectedItemSlot, expectedArmorType, expectedItemName, expectedItemLevel, expectedArmorStats);

        ItemSlot actualItemSlot = armor.getItemSlot();
        ArmorType actualArmorType = armor.getArmorType();
        GameAttributes actualArmorStats = armor.getArmorStats();
        String actualArmorName = armor.getName();
        int actualItemLevel = armor.getItemLevel();

        Assertions.assertTrue(expectedArmorStats.equals(actualArmorStats));
        Assertions.assertEquals(expectedArmorType, actualArmorType);
        Assertions.assertEquals(expectedItemSlot, actualItemSlot);
        Assertions.assertEquals(expectedItemName, actualArmorName);
        Assertions.assertEquals(expectedItemLevel, actualItemLevel);
    }
}
