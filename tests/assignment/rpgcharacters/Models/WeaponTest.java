package assignment.rpgcharacters.Models;

import assignment.rpgcharacters.Factories.ItemFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.InvalidParameterException;

import static org.junit.jupiter.api.Assertions.*;

public class WeaponTest {

    private static Weapon weapon;

    @BeforeEach
    void initialiseTest() {
        weapon = ItemFactory.CreateAxe("Worn Axe", 1, 3, 1.8);
    }

    // test constructor
    @Test
    void weaponConstructor_generatesWeaponWithExpectedInput() {
        weapon = ItemFactory.CreateAxe("Axe of Doom",5, 10, 0.8);
        String expectedName = "Axe of Doom";
        int expectedMinDmg = 5;
        int expectedMaxDmg = 10;
        double expectedAttackSpeed = 0.8;

        assertEquals(expectedName, weapon.getName());
        assertEquals(expectedMinDmg, weapon.getMinDmg());
        assertEquals(expectedMaxDmg, weapon.getMaxDmg());
        assertEquals(expectedAttackSpeed, weapon.getAttackSpeed());
    }

    // test setters
    @Test
    void setAttackSpeed_invalidInput_throwsException() {
        assertThrows(InvalidParameterException.class, () -> weapon.setAttackSpeed(-1));
        assertThrows(InvalidParameterException.class, () -> weapon.setAttackSpeed(10.01));
        assertThrows(InvalidParameterException.class, () -> weapon.setAttackSpeed(0));
    }

    @Test
    void setAttackSpeed_validInput_setsAttackSpeed() {
        double expected = 0.9;

        weapon.setAttackSpeed(0.9);
        double actual = weapon.getAttackSpeed();

        assertEquals(expected, actual);
    }

    // test damage range
    @Test
    void getDamage_operatesWithinMinAndMaxValues_returnsDamageBetweenTwoValues() {
        // act
        weapon.setDamage(2, 3);
        int expectedDmgEitherTwoOrThree = weapon.getWeaponDamage();

        // assert
        if (expectedDmgEitherTwoOrThree == 2 || expectedDmgEitherTwoOrThree == 3) {
            assertTrue(true, String.valueOf(expectedDmgEitherTwoOrThree));
            return;
        }

        fail("Unexpected damage: " + expectedDmgEitherTwoOrThree);
    }

    // test calculationOfDps
    @Test
    void weaponGetDps_returnsRoundedDownInt() {
        weapon.setDamage(1, 2);
        weapon.setAttackSpeed(1);

        int actualDps = weapon.getWeaponDps();

        if (actualDps == 1 || actualDps == 2) {
            return; // pass
        }
        fail("actualDps was not 1 or 2");
    }

    @Test
    void setWeaponDamage_invalidInput_throwsParameterException() {
        assertThrows(InvalidParameterException.class, () -> weapon.setDamage(5, 2), "minDmg cannot be larger than maxDmg");
        assertThrows(InvalidParameterException.class, () -> weapon.setDamage(-1, 2), "minDmg cannot have negative values");
    }
}
