package assignment.rpgcharacters.Models;

import assignment.rpgcharacters.Exceptions.InvalidArmorException;
import assignment.rpgcharacters.Exceptions.InvalidWeaponException;
import assignment.rpgcharacters.Factories.CharacterFactory;
import assignment.rpgcharacters.Factories.ItemFactory;
import assignment.rpgcharacters.Models.Enums.ArmorType;
import assignment.rpgcharacters.Models.Interfaces.IItem;
import assignment.rpgcharacters.Models.Interfaces.IMage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    private IMage mage;

    @BeforeEach
    void setUp() {
        mage = CharacterFactory.CreateMage("Nobus");
    }

    // test naming: operation_condition_expectedOutcome
    @Test
    public void levelUp_character_returnsLevelPlusOne() {
        int expectedLevel = 2;
        int actualLevel;

        mage.levelUp();
        actualLevel = mage.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void levelUp_character_baseStatsAreNotTheSameAfterLevelUp() {
        GameAttributes statsBeforeLevelUp = new GameAttributes(mage.getBaseAttributes());

        mage.levelUp();
        GameAttributes statsAfterLevelUp = mage.getBaseAttributes();

        assertFalse(statsBeforeLevelUp.equals(statsAfterLevelUp));
    }

    @Test
    public void characterConstructor_validInput_isLevelOne() {
        int expectedLevel = 1;
        int actualLevel = mage.getLevel();

        assertEquals(expectedLevel, actualLevel);
    }

    @Test
    public void equipItem_validArmor_returnsTrue() {
        GameAttributes stats = new GameAttributes(0, 2, 4);
        Armor bodyArmor = ItemFactory.CreateBodyArmor(ArmorType.CLOTH, "Robe", stats);
        boolean expected = true;

        boolean actual = mage.equipItem(bodyArmor);

        assertEquals(expected, actual);
    }

    @Test
    public void equipItem_validWeapon_returnsTrue() {
        // arrange
        mage.levelUp(); // makes it a lvl 2 mage
        IItem myWeapon = ItemFactory.CreateStaff("Conjurer's Staff", 2, 2, 7, 0.5);
        boolean expected = true;
        // act
        boolean actual = mage.equipItem(myWeapon);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void equipItem_invalidWeaponType_throwsInvalidWeaponException() {
        // arrange
        IItem myWeapon = ItemFactory.CreateAxe("War-bringer Axe", 2, 2, 7, 0.5);
        // act
        assertThrows(InvalidWeaponException.class, () -> mage.equipItem(myWeapon));
    }

    @Test
    public void equipItem_invalidArmorType_throwsInvalidArmorException() {
        // arrange
        Armor myArmor = ItemFactory.CreateLegArmor(ArmorType.PLATE, "Chestplate", 4, new GameAttributes(1, 1, 1));

        // act
        assertThrows(InvalidArmorException.class, () -> mage.equipItem(myArmor));
    }

    @Test
    void equipItem_tooHighLevelWeapon_throwsInvalidWeaponException() {
        // arrange
        IItem myWeapon = ItemFactory.CreateStaff("Conjurer's Staff", 2, 2, 7, 0.5);
        // act & assert
        assertThrows(InvalidWeaponException.class, () -> mage.equipItem(myWeapon));
    }

    @Test
    void equipItem_tooHighLevelArmor_throwsInvalidArmorException() {
        // arrange
        Armor myArmor = ItemFactory.CreateLegArmor(ArmorType.CLOTH, "Kilt", 4, new GameAttributes(1, 1, 1));
        // act & assert
        assertThrows(InvalidArmorException.class, () -> mage.equipItem(myArmor));
    }

    @Test
    void getTotalAttributes_withOutEquipment_returnsBaseGameAttributes() {
        GameAttributes base = mage.getBaseAttributes();
        GameAttributes total = mage.getTotalAttributes();
        assertTrue(base.equals(total));
    }

    @Test
    void getTotalAttributes_withEquipment_returnsCombinedGameAttributes() {
        // arrange
        GameAttributes crownStats = new GameAttributes(1, 1, 2);
        Armor crown = ItemFactory.CreateHeadArmor(ArmorType.CLOTH, "Crown of Deception", crownStats);
        // lvl 1 mage base stats + item stats
        int expectedStr = 2; // base + 1
        int expectedDex = 2; // base + 1
        int expectedInt = 10; // base + 2

        // act
        mage.equipItem(crown);
        GameAttributes mageTotalAttributes = mage.getTotalAttributes();

        // assert
        assertEquals(expectedStr, mageTotalAttributes.getStrength());
        assertEquals(expectedDex, mageTotalAttributes.getDexterity());
        assertEquals(expectedInt, mageTotalAttributes.getIntelligence());
    }

    @Test
    void toString_returnsCharacterSheet() {
        // mage's name, level and totalStats.
        String expectedCharacterSheet =
                "Name: " + mage.getName() + "\n" +
                        "Class: Mage\n" +
                        "Level: 1\n" +
                        "Strength: 1\n" +
                        "Dexterity: 1\n" +
                        "Intelligence: 8\n";

        String actual = mage.toString();

        assertEquals(expectedCharacterSheet, actual);
    }

    @Test
    void calculateDps_lvl1MageNoEquipment_returns1() {
        int expectedDmg = 1;
        int actualDmg = mage.calculateDps();
        assertEquals(expectedDmg, actualDmg);
    }

    @Test
    void calculateDps_lvl1MageWithWeapon_returnsInt() {
        Weapon wand = ItemFactory.CreateWand("Simple Wand", 12, 12, 1);
        int expectedDmg = wand.getWeaponDps() * (1 + mage.getTotalAttributes().getPrimaryAttributeValue() / 100); // 14
        mage.equipItem(wand);

        int actualDmg = mage.calculateDps();

        assertEquals(expectedDmg, actualDmg);
    }
    @Test
    void calculateDps_lvl1MageWithWeaponAndArmor_returnsInt() {
        Weapon wand = ItemFactory.CreateWand("Simple Wand", 2, 2, 1);
        GameAttributes crownStats = new GameAttributes(0, 0, 200);
        Armor crown = ItemFactory.CreateHeadArmor(ArmorType.CLOTH, "Crown of Deception", crownStats);
        mage.equipItem(wand);
        mage.equipItem(crown);
        int expectedDmg = wand.getWeaponDps() * (1 + mage.getTotalAttributes().getPrimaryAttributeValue() / 100); // 6

        int actualDmg = mage.calculateDps();

        assertEquals(expectedDmg, actualDmg);
    }
}