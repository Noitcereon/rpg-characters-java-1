package assignment.rpgcharacters.Factories;

import assignment.rpgcharacters.Models.GameAttributes;
import assignment.rpgcharacters.Models.Enums.PrimaryAttribute;
import assignment.rpgcharacters.Models.Interfaces.IRanger;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class CharacterFactoryRangerTests {

    @Test
    void createRanger_returnsExpectedDefaultStats() {
        IRanger ranger = CharacterFactory.CreateRanger("Björn Simperson");
        GameAttributes expectedStats = new GameAttributes(PrimaryAttribute.DEXTERITY, 1, 7, 1);
        GameAttributes actualStats = ranger.getBaseAttributes();

        assertTrue(actualStats.equals(expectedStats));
    }

    @Test
    void createRanger_createdCharacterGainsExpectedStatsOnLevelUp() {
        IRanger ranger = CharacterFactory.CreateRanger("Björn Simperson");
        // Level 2 ranger base stats
        GameAttributes expectedStats = new GameAttributes(PrimaryAttribute.DEXTERITY, 2, 12, 2);

        ranger.levelUp();
        GameAttributes actualStats = ranger.getBaseAttributes();

        assertTrue(actualStats.equals(expectedStats));
    }
}