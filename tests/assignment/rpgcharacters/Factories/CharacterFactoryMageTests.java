package assignment.rpgcharacters.Factories;

import assignment.rpgcharacters.Models.GameAttributes;
import assignment.rpgcharacters.Models.Enums.PrimaryAttribute;
import assignment.rpgcharacters.Models.Interfaces.IMage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class CharacterFactoryMageTests {

    @Test
    void createMage_returnsExpectedDefaultStats() {
        IMage mage = assignment.rpgcharacters.Factories.CharacterFactory.CreateMage("Björn Bjarksen");
        GameAttributes expectedStats = new GameAttributes(PrimaryAttribute.INTELLIGENCE, 1, 1, 8);
        GameAttributes actualStats = mage.getBaseAttributes();

        assertTrue(actualStats.equals(expectedStats));
    }

    @Test
    void createMage_createdCharacterGainsExpectedStatsOnLevelUp() {
        IMage mage = CharacterFactory.CreateMage("Björn Bjarksen");
        // Level 2 mage base stats
        GameAttributes expectedStats = new GameAttributes(PrimaryAttribute.INTELLIGENCE, 2, 2, 13);

        mage.levelUp();
        GameAttributes actualStats = mage.getBaseAttributes();

        assertTrue(expectedStats.equals(actualStats));
    }
}