package assignment.rpgcharacters.Factories;

import assignment.rpgcharacters.Models.GameAttributes;
import assignment.rpgcharacters.Models.Enums.PrimaryAttribute;
import assignment.rpgcharacters.Models.Interfaces.IRogue;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class CharacterFactoryRogueTests {

    @Test
    void createRogue_returnsExpectedDefaultStats() {
        IRogue rogue = CharacterFactory.CreateRogue("Björn Snorelump");
        GameAttributes expectedStats = new GameAttributes(PrimaryAttribute.DEXTERITY, 2, 6, 1);

        GameAttributes actualStats = rogue.getBaseAttributes();

        assertTrue(expectedStats.equals(actualStats));
    }

    @Test
    void createRogue_createdCharacterGainsExpectedStatsOnLevelUp() {
        IRogue rogue = CharacterFactory.CreateRogue("Björn Snorelump");
        // Level 2 rogue base stats
        GameAttributes expectedStats = new GameAttributes(PrimaryAttribute.DEXTERITY, 3, 10, 2);

        rogue.levelUp();
        GameAttributes actualStats = rogue.getBaseAttributes();

        assertTrue(expectedStats.equals(actualStats));
    }

}