package assignment.rpgcharacters.Factories;

import assignment.rpgcharacters.Models.GameAttributes;
import assignment.rpgcharacters.Models.Enums.PrimaryAttribute;
import assignment.rpgcharacters.Models.Interfaces.IWarrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class CharacterFactoryWarriorTests {


    @Test
    void createWarrior_returnsExpectedDefaultStats() {
        IWarrior warrior = CharacterFactory.CreateWarrior("Björn Eskildsson");
        GameAttributes expectedStats = new GameAttributes(PrimaryAttribute.STRENGTH, 5, 2, 1);

        GameAttributes actualStats = warrior.getBaseAttributes();

        assertTrue(expectedStats.equals(actualStats));
    }

    @Test
    void createWarrior_createdCharacterGainsExpectedStatsOnLevelUp() {
        IWarrior warrior = CharacterFactory.CreateWarrior("Björn Eskildsson");
        // Level 2 warrior base stats
        GameAttributes expectedStats = new GameAttributes(PrimaryAttribute.STRENGTH, 8, 4, 2);

        warrior.levelUp();
        GameAttributes actualStats = warrior.getBaseAttributes();

        assertTrue(expectedStats.equals(actualStats));
    }
}